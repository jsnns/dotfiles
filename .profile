echo "source ~/.profile" > ~/.zshrc
ssh-add -K
dotfiles=~/.me/conf/bash

export EDITOR=vim

for DOTFILE in `find $dotfiles`
do
	[ -f "$DOTFILE" ] && source "$DOTFILE"
done