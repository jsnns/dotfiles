alias l="ls -la"       # List in long format, include dotfiles
alias ld="ls -ld */"   # List in long format, only directories
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."

alias commit="EDITOR=vim -X git commit"

alias gohome="cd ~"
alias gossh="cd ~/.ssh"

alias cleanupds="find . -type f -name '*.DS_Store' -ls -delete"

alias resource="source ~/.profile"

alias 1="ls -la"
alias 3="vim ."
